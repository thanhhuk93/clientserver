﻿using NKSLK.DAO;
using PagedList;
using System;
using System.Web.Mvc;

namespace NKSLK.Controllers
{
    [Authorize]
    public class StatisticsController : Controller
    {
        StatisticsDAO dao = new StatisticsDAO();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult MostPrevalentWorkStatistics
            (int page = 1,
             int pageSize = 10,
             string criterion = "All",
             string selectedDate = null,
             string order = null)
        {
            if (ModelState.IsValid)
            {
                if (selectedDate == null)
                {
                    if (criterion == "Week")
                    {
                        selectedDate = DateTime.Today.ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        selectedDate = DateTime.Today.ToString("MM/yyyy");
                    }
                }
                var model = dao.GetMostPrevalentWorks(criterion, selectedDate, order);
                model = model.ToPagedList(page, pageSize);

                ViewBag.page = page;
                ViewBag.pageSize = pageSize;
                ViewBag.criterion = criterion;
                ViewBag.selectedDate = selectedDate;
                ViewBag.ID_Order = order == "ID_asc" ? "ID_desc" : "ID_asc";
                ViewBag.Name_Order = order == "Name_asc" ? "Name_desc" : "Name_asc";
                ViewBag.Quantity_Order = String.IsNullOrEmpty(order) ? "Quantity_asc" : "";
                ViewBag.order = order;

                return View(model);
            }
            else
            {
                ModelState.AddModelError("", "Error");
            }

            return View();
        }

        public ActionResult ThirdShiftStatistics
           (int page = 1,
            int pageSize = 10,
            string criterion = "All",
            string selectedDate = null,
            string order = null)
        {
            if (ModelState.IsValid)
            {
                if (selectedDate == null)
                {
                    if (criterion == "Week")
                    {
                        selectedDate = DateTime.Today.ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        selectedDate = DateTime.Today.ToString("MM/yyyy");
                    }
                }
                var model = dao.GetThirdShifts(criterion, selectedDate, order);
                model = model.ToPagedList(page, pageSize);

                ViewBag.page = page;
                ViewBag.pageSize = pageSize;
                ViewBag.criterion = criterion;
                ViewBag.selectedDate = selectedDate;
                ViewBag.ID_Order = String.IsNullOrEmpty(order) ? "ID_desc" : "";
                ViewBag.Name_Order = order == "Name_asc" ? "Name_desc" : "Name_asc";
                ViewBag.order = order;

                return View(model);
            }
            else
            {
                ModelState.AddModelError("", "Error");
            }

            return View();
        }

        public ActionResult NKSLKStatistics
            (int page = 1,
             int pageSize = 10,
             string criterion = "All",
             string selectedDate = null,
             string search = null,
             int idLabour = 0,
             string order = null)
        {
            if (ModelState.IsValid)
            {               
                if (selectedDate == null)
                {
                    if (criterion == "Week")
                    {
                        selectedDate = DateTime.Today.ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        selectedDate = DateTime.Today.ToString("MM/yyyy");
                    }
                }
                var model = dao.GetNKSLKs(criterion, selectedDate, search, idLabour, order);
                model = model.ToPagedList(page, pageSize);

                ViewBag.page = page;
                ViewBag.pageSize = pageSize;
                ViewBag.criterion = criterion;
                ViewBag.search = search;
                ViewBag.selectedDate = selectedDate;
                ViewBag.labourList = new SelectList(dao.GetLabourList(), "ID_CONGNHAN", "HoTen");
                ViewBag.idLabour = idLabour;
                ViewBag.ID_Order = order == "ID_asc" ? "ID_desc" : "ID_asc";
                ViewBag.LabourName_Order = order == "LabourName_asc" ? "LabourName_desc" : "LabourName_asc";
                ViewBag.Day_Order = String.IsNullOrEmpty(order) ? "Day_desc" : "";
                ViewBag.Start_Order = order == "Start_asc" ? "Start_desc" : "Start_asc";
                ViewBag.End_Order = order == "End_asc" ? "End_desc" : "End_asc";
                ViewBag.WorkName_Order = order == "WorkName_asc" ? "WorkName_desc" : "WorkName_asc";
                ViewBag.ProductName_Order = order == "ProductName_asc" ? "ProductName_desc" : "ProductName_asc";
                ViewBag.Quantity_Order = order == "Quantity_asc" ? "Quantity_desc" : "Quantity_asc";
                ViewBag.Batch_Order = order == "Batch_asc" ? "Batch_desc" : "Batch_asc";
                ViewBag.order = order;

                return View(model);
            }
            else
            {
                ModelState.AddModelError("", "Error");
            }

            return View();
        }

        public ActionResult SalaryStatistics
            (int page = 1,
             int pageSize = 10,
             string criterion = "Week",
             string selectedDate = null,
             string order = null)
        {
            if (ModelState.IsValid)
            {
                if (selectedDate == null)
                {
                    if (criterion == "Week")
                    {
                        selectedDate = DateTime.Today.ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        selectedDate = DateTime.Today.ToString("MM/yyyy");
                    }
                }
                var model = dao.GetSalaries(criterion, selectedDate, order);
                model = model.ToPagedList(page, pageSize);

                ViewBag.page = page;
                ViewBag.pageSize = pageSize;
                ViewBag.criterion = criterion;
                ViewBag.selectedDate = selectedDate;
                ViewBag.ID_Order = String.IsNullOrEmpty(order) ? "ID_desc" : "";
                ViewBag.Name_Order = order == "Name_asc" ? "Name_desc" : "Name_asc";
                ViewBag.Salary_Order = order == "Salary_asc" ? "Salary_desc" : "Salary_asc";
                ViewBag.order = order;

                return View(model);
            }
            else
            {
                ModelState.AddModelError("", "Error");
            }

            return View();
        }

        public ActionResult WorkdayStatistics
            (int page = 1,
             int pageSize = 10,
             string selectedDate = null,
             int idLabour = 0,
             string order = null)
        {

            if (ModelState.IsValid)
            {
                if (selectedDate == null)
                {
                    selectedDate = DateTime.Today.ToString("MM/yyyy");
                }
                var model = dao.GetWorkdays(selectedDate, idLabour, order);
                model = model.ToPagedList(page, pageSize);

                ViewBag.page = page;
                ViewBag.pageSize = pageSize;
                ViewBag.selectedDate = selectedDate;
                ViewBag.labourList = new SelectList(dao.GetLabourList(), "ID_CONGNHAN", "HoTen");
                ViewBag.idLabour = idLabour;
                ViewBag.ID_Order = String.IsNullOrEmpty(order) ? "ID_desc" : "";
                ViewBag.Name_Order = order == "Name_asc" ? "Name_desc" : "Name_asc";
                ViewBag.Workday_Order = order == "Workday_asc" ? "Workday_desc" : "Workday_asc";
                ViewBag.order = order;

                return View(model);
            }
            else
            {
                ModelState.AddModelError("", "Error");
            }

            return View();
        }

        public ActionResult WorkinghourStatistics
            (int page = 1,
             int pageSize = 10,
             string selectedDate = null,
             string order = null)
        {
            if (ModelState.IsValid)
            {
                if (selectedDate == null)
                {
                    selectedDate = DateTime.Today.ToString("dd/MM/yyyy");
                }
                var model = dao.GetWorkinghours(selectedDate, order);
                model = model.ToPagedList(page, pageSize);

                ViewBag.page = page;
                ViewBag.pageSize = pageSize;
                ViewBag.selectedDate = selectedDate;
                ViewBag.ID_Order = String.IsNullOrEmpty(order) ? "ID_desc" : "";
                ViewBag.Name_Order = order == "Name_asc" ? "Name_desc" : "Name_asc";
                ViewBag.Workinghour_Order = order == "Workinghour_asc" ? "Workinghour_desc" : "Workinghour_asc";
                ViewBag.order = order;

                return View(model);
            }
            else
            {
                ModelState.AddModelError("", "Error");
            }
            return View();
        }
    }
}