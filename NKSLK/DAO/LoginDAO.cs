﻿using NKSLK.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace NKSLK.DAO
{
    public class LoginDAO
    {
        NKSLKDbContext db = null;
        private static string connectionString = "data source=PC-98\\MSQSQLSERVER039;initial catalog=NKSLK;integrated security=True;" +
                                                 "MultipleActiveResultSets=True;App=EntityFramework";

        public LoginDAO()
        {
            db = new NKSLKDbContext();
        }

        public bool IsValidate(string username, string password)
        {
            string HashPassword = MD5Encryption.CreateMD5Hash(password);
            var res = db.ACCOUNTs.Count(x => x.TenAccount == username && x.MatKhau == HashPassword);
            return res > 0;
        }

        public bool IsUserExisted(string username)
        {
            var res = db.ACCOUNTs.Count(x => x.TenAccount == username);
            return res > 0;
        }

        public void SendResetPasswordEmail(string ToEmailAddress, string callbackUrl)
        {
            var FromEmailAddress = ConfigurationManager.AppSettings["FromEmailAddress"].ToString();
            var FromEmailPassword = ConfigurationManager.AppSettings["FromEmailPassword"].ToString();
            var FromEmailDisplayName = ConfigurationManager.AppSettings["FromEmailDisplayName"].ToString();
            var SMTPHost = ConfigurationManager.AppSettings["SMTPHost"].ToString();
            var SMTPPort = ConfigurationManager.AppSettings["SMTPPort"].ToString();
            bool EnabledSSL = bool.Parse(ConfigurationManager.AppSettings["EnabledSSL"].ToString());
            string content = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/template/resetpassword.html"));
            content = content.Replace("{{Link}}", callbackUrl);

            MailMessage message = new MailMessage(new MailAddress(FromEmailAddress, FromEmailDisplayName), new MailAddress(ToEmailAddress))
            {
                Subject = "Quên mật khẩu",
                Body = content,
                IsBodyHtml = true
            };

            var client = new SmtpClient
            {
                Credentials = new NetworkCredential(FromEmailAddress, FromEmailPassword),
                Host = SMTPHost,
                Port = !string.IsNullOrEmpty(SMTPPort) ? Convert.ToInt32(SMTPPort) : 0,
                EnableSsl = EnabledSSL
            };
            client.Send(message);
        }

        public void ResetPassword(string username, string newpass)
        {
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            SqlCommand command = new SqlCommand("Update ACCOUNT set MatKhau = @newpass where TenAccount = @username", connection);
            command.Parameters.Add("@username", SqlDbType.VarChar);
            command.Parameters["@username"].Value = username;
            command.Parameters.Add("@newpass", SqlDbType.VarChar);
            command.Parameters["@newpass"].Value = MD5Encryption.CreateMD5Hash(newpass);
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
}