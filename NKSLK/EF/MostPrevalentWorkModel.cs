﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NKSLK.EF
{
    public class MostPrevalentWorkModel
    {
        [Display(Name = "Mã công việc")]
        public string ID_CONGVIEC { get; set; }

        [Display(Name = "Tên công việc")]
        public string TenCongViec { get; set; }

        [Display(Name = "Số lượng")]
        public int? SoLuong { get; set; }
    }
}