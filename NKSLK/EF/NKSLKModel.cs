﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NKSLK.EF
{
    public class NKSLKModel
    {
        [Display(Name = "Mã công nhân")]
        public string ID_CONGNHAN { get; set; }

        [Display(Name = "Họ tên")]
        public string HoTen { get; set; }

        [Display(Name = "Ngày khoán")]
        public DateTime? NgayThucHienKhoan { get; set; }

        [Display(Name = "Giờ bắt đầu")]
        public TimeSpan GioBatDau { get; set; }

        [Display(Name = "Giờ kết thúc")]
        public TimeSpan GioKetThuc { get; set; }

        [Display(Name = "Tên công việc")]
        public string TenCongViec { get; set; }

        [Display(Name = "Tên sản phẩm")]
        public string TenSanPham { get; set; }

        [Display(Name = "Sản lượng")]
        public int? SanLuong { get; set; }

        [Display(Name = "Số lô")]
        public int? SoLoSanPham { get; set; }
    }
}