namespace NKSLK.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CHITIETCONGNHAN")]
    public partial class CHITIETCONGNHAN
    {
        [Key]
        public int ID_CTCONGNHAN { get; set; }

        public int ID_CONGNHAN { get; set; }

        public int ID_CTCONGVIEC { get; set; }

        public TimeSpan GioVaoLam { get; set; }

        public TimeSpan GioTanLam { get; set; }

        public virtual CONGNHAN CONGNHAN { get; set; }

        public virtual CHITIETCONGVIEC CHITIETCONGVIEC { get; set; }
    }
}
