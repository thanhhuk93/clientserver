namespace NKSLK.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CONGVIEC")]
    public partial class CONGVIEC
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CONGVIEC()
        {
            CHITIETCONGVIECs = new HashSet<CHITIETCONGVIEC>();
        }

        [Key]
        public int ID_CONGVIEC { get; set; }

        [StringLength(50)]
        public string TenCongViec { get; set; }

        [StringLength(50)]
        public string DonViKhoan { get; set; }

        public decimal? HeSoKhoan { get; set; }

        public int? DinhMucLaoDong { get; set; }

        public int? DinhMucKhoan { get; set; }

        public int? DonGia { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CHITIETCONGVIEC> CHITIETCONGVIECs { get; set; }
    }
}
