﻿
$(document).ready(function () {
    if ($("#criterion").val() == "All") {
        $("#selectedDate").attr('disabled', 'disabled');
        $("#selectedDate").css("backgroundColor", "#e9ecef");
        $("#selectedDate").val('');
    } else {
        $("#selectedDate").removeAttr('disabled');
        $("#selectedDate").css("backgroundColor", "#ffffff");
        if ($("#criterion").val() == "Week") {
            $(".date-picker").datepicker("destroy");
            $(".date-picker").datepicker({
                dateFormat: 'dd/mm/yy',
                changeMonth: true,
                changeYear: true,
                showWeek: true,
                firstDay: 1
            });
            $(".date-picker").focus(function () {
                $(".ui-datepicker-calendar").show();
            });
            if ($("#selectedDate").val() == "") {
                $(".date-picker").datepicker("setDate", "today");
            }
        } else {
            $(".date-picker").datepicker("destroy");
            $(".date-picker").datepicker({
                dateFormat: 'mm/yy',
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                onClose: function () {
                    var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                    var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                    $(this).val($.datepicker.formatDate('mm/yy', new Date(year, month, 1)));
                }
            });
            $(".date-picker").focus(function () {
                $(".ui-datepicker-calendar").hide();
            });
            if ($("#selectedDate").val() == "") {
                $(".date-picker").datepicker("setDate", "today");
            }
        }
    }
    $("#criterion").change(function () {
        if ($("#criterion").val() == "All") {
            $("#selectedDate").attr('disabled', 'disabled');
            $("#selectedDate").css("backgroundColor", "#e9ecef");
            $("#selectedDate").val('');
        } else {
            $("#selectedDate").removeAttr('disabled');
            $("#selectedDate").css("backgroundColor", "#ffffff");
            if ($("#criterion").val() == "Week") {
                $(".date-picker").datepicker("destroy");
                $(".date-picker").datepicker({
                    dateFormat: 'dd/mm/yy',
                    changeMonth: true,
                    changeYear: true,
                    showWeek: true,
                    firstDay: 1
                });
                $(".date-picker").focus(function () {
                    $(".ui-datepicker-calendar").show();
                });
                $(".date-picker").datepicker("setDate", "today");
            } else {
                $(".date-picker").datepicker("destroy");
                $(".date-picker").datepicker({
                    dateFormat: 'mm/yy',
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                    onClose: function () {
                        var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                        var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                        $(this).val($.datepicker.formatDate('mm/yy', new Date(year, month, 1)));
                    }
                });
                $(".date-picker").focus(function () {
                    $(".ui-datepicker-calendar").hide();
                });
                $(".date-picker").datepicker("setDate", "today");
            }
        }
    });
});

$(document).ready(function () {
    $(".date-picker2").datepicker("destroy");
    $(".date-picker2").datepicker({
        dateFormat: 'mm/yy',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        onClose: function () {
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).val($.datepicker.formatDate('mm/yy', new Date(year, month, 1)));
        }
    });
    $(".date-picker2").focus(function () {
        $(".ui-datepicker-calendar").hide();
    });
    if ($("#selectedDate").val() == "") {
        $(".date-picker").datepicker("setDate", "today");
    }
});

$(document).ready(function () {
    $(".date-picker3").datepicker("destroy");
    $(".date-picker3").datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true,
        showWeek: true,
        firstDay: 1
    });
    $(".date-picker3").focus(function () {
        $(".ui-datepicker-calendar").show();
    });
    if ($("#selectedDate").val() == "") {
        $(".date-picker").datepicker("setDate", "today");
    }
});